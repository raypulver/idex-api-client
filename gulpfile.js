'use strict';

const {
  bindKey,
  partial
} = require('lodash');
const gulp = require('gulp');
const task = bindKey(gulp, 'task');
const src = bindKey(gulp, 'src');
const { readFileSync } = require('fs');
const jshintrc = JSON.parse(readFileSync('./.jshintrc'));
const jshint = require('gulp-jshint');

task('jshint', () => src('./lib/**/*.js')
  .pipe(jshint(jshintrc))
  .pipe(jshint.reporter('jshint-stylish')));
