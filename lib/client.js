'use strict';

const {
  fromV3
} = require('ethereumjs-wallet');
const {
  toBuffer,
  bufferToHex,
  ecsign
} = require('ethereumjs-util');
const mapValues = require('lodash/mapValues');
const subscriber = require('./subscriber');
const hash = require('./hash');
const assign = require('object-assign');
const api = require('./api');
const hashInterface = require('./hash-interface');

function IDEXMethods() {}

assign(IDEXMethods.prototype, mapValues(api, (v, k) => function (payload) {
  if (hashInterface[k]) return v(assign(mapValues(ecsign(toBuffer(hash.saltedHash(k, assign({ contractAddress: this.getContractAddress() }, payload)))), (v, k) => k === 'v' ? v : bufferToHex(v)), payload));
  return v(payload);
}));

assign(IDEXMethods.prototype, [
  'subscribe',
  'unsubscribe',
  'on',
  'emit'
].reduce((r, v) => {
  r[v] = function (...args) {
    return this.getSubscriber()[v](...args);
  };
  return r;
}));

class IDEXClient extends IDEXMethods {
  constructor(wallet, password) {
    super();
    this.setWallet(fromV3(wallet, password));
    this.setSubscriber(new subscriber.Subscriber());
  }
  setSubscriber(s) {
    this._subscriber = s;
  }
  storeContractAddress() {
    return this.returnContractAddress().then((address) => (this.setContractAddress(address)));
  }
  getSubscriber() {
    return this._subscriber;
  }
  setWallet(v3) {
    this._v3 = v3;
  }
  getWallet() {
    return this._v3;
  }
}

const createIDEXClient = (...args) => {
  const client = new IDEXClient(...args);
  return client.storeContractAddress().then(() => client);
};

module.exports = createIDEXClient; 
