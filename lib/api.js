'use strict';

const XMLHttpRequest = require('xmlhttprequest').XMLHttpRequest;
/* jshint ignore:start */
const Promise = require('bluebird');
/* jshint ignore:end */
const partial = require('lodash/partial');
const assign = require('object-assign');
const {
  url
} = require('../config');
const fetch = ({
  method,
  url,
  json
}) => new Promise((resolve, reject) => {
  const xhr = new XMLHttpRequest();
  xhr.open('POST', url);
  xhr.setRequestHeader('Content-type', 'application/json;charset=utf-8');
  xhr.addEventListener('readystatechange', () => {
    if (xhr.readyState === 4 && xhr.status === 200) {
      try {
        resolve(JSON.parse(xhr.responseText));
      } catch (err) { reject(err); }
    }
  });
  xhr.addEventListener('error', reject);
  xhr.send(JSON.stringify(json));
});

const apiCall = (method, payload = {}) => fetch({
  url: url + '/' + method,
  json: payload,
  method: 'POST'
}).then((response) => {
  if (response.error) throw Error(response.error);
  return response;
});

const methods = [
  'order',
  'return24Volume',
  'returnBalances',
  'returnCompleteBalances',
  'returnContractAddress',
  'returnCurrencies',
  'returnDepositsWithdrawals',
  'returnNextNonce',
  'returnOpenOrders',
  'returnOrderBook',
  'returnOrderTrades',
  'returnTicker',
  'returnTradeHistory',
  'trade',
  'withdraw'
];

assign(module.exports, methods.reduce((r, v) => {
  r[v] = partial(apiCall, v);
  return r;
}, {}), {
  apiCall,
  fetch
});
