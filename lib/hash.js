'use strict';

const hashInterface = require('./hash-interface');
const assign = require('object-assign');
const {
  hashPersonalMessage,
  toBuffer,
  addHexPrefix,
  bufferToHex
} = require('ethereumjs-util');
const { 
  soliditySha3
} = require('web3-utils');
  

const hashObject = (method, o) => {
  const raw = soliditySha3(...hashInterface[method].map(({ name, type }) => ({
    t: type,
    v: o[name]
  })));
  const salted = bufferToHex(hashPersonalMessage(toBuffer(raw)));
  return { raw, salted };
};

const saltedHash = (method, o) => hashObject(method, o).salted;

const verifyOrder = (contractAddress, order) => {
  const { raw } = hashObject('order', Object.assign({ contractAddress }, order.params));
  if (raw !== order.orderHash) throw Error('Invalid hash for order');
};

assign(module.exports, {
  hashObject,
  verifyOrder,
  saltedHash
});
