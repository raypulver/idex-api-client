'use strict';

const WebSocket = require('websocket');
const { v1 } = require('uuid');
const { url } = require('../config');
const EventEmitter = require('event-emitter');
const { mapSeries } = require('bluebird');
const assign = require('object-assign');

const defer = () => {
  let resolve, reject, promise = new Promise((r, rj) => {
    resolve = r;
    reject = rj;
  });
  return {
    resolve,
    reject,
    promise
  };
};

class Subscriber extends EventEmitter {
  constructor() {
    super();
    this._init();
  }
  _initDeferredMap() {
    this._map = {};
  }
  getDeferredMap() {
    return this._map;
  }
  getNewDeferred() {
    const id = v1();
    const deferred = defer();  
    this.getDeferredMap()[id] = deferred;
    return {
      id,
      promise: deferred.promise,
      resolve: deferred.resolve,
      reject: deferred.reject
    };
  }
  isConnected() {
    return this._connected;
  }
  setConnected(flag) {
    this._connected = flag;
  }
  _bindHandlers() {
    const sock = this.getSocket();
    sock.addEventListener('open', () => this.handleOpen());
    sock.addEventListener('close', (e) => {
      this.setConnected(false);
      switch (e) {
        case 1000:
          break;
        default:
          this.queueReconnect();
          break;
      }
    });
    sock.addEventListener('error', (e) => {
      switch (e.code) {
        case 'ECONNREFUSED':
          this.setConnected(false);
          return this.queueReconnect();
        default:
          break;
      }
    });
    sock.addEventListener('message', (m) => this.handleMessage(m));
  }
  queue(job) {
    if (!this.getConnected()) this._queue.push(job);
    else job();
  }
  getQueue() {
    return this._queue;
  }
  flushQueue() {
    mapSeries(this._queue, (v) => v());
    this._queue = [];
  }
  send(payload) {
    const deferred = this.getNewDeferred();
    payload.id = deferred.id;
    this.queue(() => this.getSocket().send(JSON.stringify(payload)));
    return deferred.promise;
  }
  handleOpen() {
    this.flushQueue();
  }
  handleMessage(m) {
    this.emit('raw', m);
    try {
      const payload = JSON.parse(m);
      this.emit('payload', payload);
      const { id, topic, error } = payload;
      const deferred = this.getDeferredMap()[id];
      if (topic) this.emit('message', payload);
      if (deferred) {
        if (error) return deferred.reject(Error(error));
        deferred.resolve(payload);
      } 
    } catch (e) {}
  }
  _init() {
    this._initDeferredMap();
    this._queue = [];
    this.connect();
  }
  connect() {
    if (this.getSocket()) {
      try {
        this.getSocket().close();
      } catch (e) {}
    }
    this.setSocket(new WebSocket(url));
    this.bindHandlers();
  }
  setSocket(s) {
    this._sock = s;
  }
  getSocket() {
    return this._sock;
  }
  queueReconnect() {
    setTimeout(() => this.connect(), 5000);
  } 
  subscribe(topic) {
    return this.send({
      subscribe: topic
    });
  }
}

assign(module.exports, { Subscriber });
